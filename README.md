# sgx-dlib

- Applying our mirroring mechanism to a machine learning use case with the Intel SGX SDK, sgx-romulus, and dlib machine learning library.
- This `master` branch contains only working code. The main work is done on the `sgx-port` branch.
- The program on this branch is a digit recognition machine learning program based on the `mnist` dataset. It builds and trains a model to do digit recognition. 
- The example here simply does training and inference in the untrusted runtime. 
- Training is been tested inside the enclave on the `sgx-port` branch. 

# How to test 
- Install the Intel SGX SDK and driver on your system.
- Clone this repo.
- `cd sgx-dlib && make`
- Run the training program with: `./app data/mnist`.
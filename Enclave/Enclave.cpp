/**
 * Author: Peterson Yuhala
 * sgx-remus: sgx compatible nvram library based on romulus
 */

#include "Enclave.h"
#include "sgx_trts.h"
#include "sgx_thread.h" //for thread manipulation
#include "Enclave_t.h"  /* print_string */
#include <stdarg.h>
#include <stdio.h>

//#include <thread>
#include "romulus/datastructures/TMStack.hpp"

//romAttrib *romAttrib_out; //contains marshalled properties of outside romulus object
uint8_t *base_addr_in = NULL; //this will receive the value of the


void ecall_init(void *per_out, uint8_t *base_addr_out)
{
    //pointer safety/validity checks: TODO

    base_addr_in = base_addr_out;
    if (base_addr_in == NULL)
    {
        sgx_printf("Base addr null, exiting..\n");
        return;
    }
    romuluslog::gRomLog.per = (PersistentHeader *)per_out;
    sgx_printf("Pmem state: %d\n", romuluslog::gRomLog.per->id); //for testing

    romuluslog::gRomLog.ns_init(); //normally private..

    sgx_printf("Enclave init success..init base address for pmem\n");

    // Create an empty stack in PM and save the root pointer (index 0) to use in a later tx
    TM_WRITE_TRANSACTION([&]() {
        PStack *pstack = RomulusLog::get_object<PStack>(0);
        if (pstack == nullptr)
        {
            sgx_printf("Creating persistent stack...\n");
            PStack *pstack = (PStack *)TM_PMALLOC(sizeof(struct PStack));
            RomulusLog::put_object(0, pstack);
        }
        else
        {
            sgx_printf("Persistent stack exists...\n");
        }
    });
}

/* Model trainer */
/*void ecall_model_trainer(){
    // Now let's define the LeNet.  Broadly speaking, there are 3 parts to a network
    // definition.  The loss layer, a bunch of computational layers, and then an input
    // layer.  You can see these components in the network definition below.
    //
    // The input layer here says the network expects to be given matrix<unsigned char>
    // objects as input.  In general, you can use any dlib image or matrix type here, or
    // even define your own types by creating custom input layers.
    //
    // Then the middle layers define the computation the network will do to transform the
    // input into whatever we want.  Here we run the image through multiple convolutions,
    // ReLU units, max pooling operations, and then finally a fully connected layer that
    // converts the whole thing into just 10 numbers.
    //
    // Finally, the loss layer defines the relationship between the network outputs, our 10
    // numbers, and the labels in our dataset.  Since we selected loss_multiclass_log it
    // means we want to do multiclass classification with our network.   Moreover, the
    // number of network outputs (i.e. 10) is the number of possible labels.  Whichever
    // network output is largest is the predicted label.  So for example, if the first
    // network output is largest then the predicted digit is 0, if the last network output
    // is largest then the predicted digit is 9.
    using net_type = loss_multiclass_log<
        fc<10,
           relu<fc<84,
                   relu<fc<120,
                           max_pool<2, 2, 2, 2, relu<con<16, 5, 5, 1, 1, max_pool<2, 2, 2, 2, relu<con<6, 5, 5, 1, 1, input<matrix<unsigned char>>>>>>>>>>>>>>;
    // This net_type defines the entire network architecture.  For example, the block
    // relu<fc<84,SUBNET>> means we take the output from the subnetwork, pass it through a
    // fully connected layer with 84 outputs, then apply ReLU.  Similarly, a block of
    // max_pool<2,2,2,2,relu<con<16,5,5,1,1,SUBNET>>> means we apply 16 convolutions with a
    // 5x5 filter size and 1x1 stride to the output of a subnetwork, then apply ReLU, then
    // perform max pooling with a 2x2 window and 2x2 stride.

    // So with that out of the way, we can make a network instance.
    net_type net;
    // And then train it using the MNIST data.  The code below uses mini-batch stochastic
    // gradient descent with an initial learning rate of 0.01 to accomplish this.
    dnn_trainer<net_type> trainer(net);
    trainer.set_learning_rate(0.01);
    trainer.set_min_learning_rate(0.00001);
    trainer.set_mini_batch_size(128);
    trainer.be_verbose();
    // Since DNN training can take a long time, we can ask the trainer to save its state to
    // a file named "mnist_sync" every 20 seconds.  This way, if we kill this program and
    // start it again it will begin where it left off rather than restarting the training
    // from scratch.  This is because, when the program restarts, this call to
    // set_synchronization_file() will automatically reload the settings from mnist_sync if
    // the file exists.
    trainer.set_synchronization_file("mnist_sync", std::chrono::seconds(20));
    // Finally, this line begins training.  By default, it runs SGD with our specified
    // learning rate until the loss stops decreasing.  Then it reduces the learning rate by
    // a factor of 10 and continues running until the loss stops decreasing again.  It will
    // keep doing this until the learning rate has dropped below the min learning rate
    // defined above or the maximum number of epochs as been executed (defaulted to 10000).
    trainer.train(training_images, training_labels);
}*/

/* Worker: core data structure manipulations initialize from here */
void ecall_nvram_worker(int val, size_t tid)
{
    //start worker: worker pushes and pops values from the start
    do_work(val, tid);
}

void do_work(int val, size_t tid)
{

    //Pops a value from the persistent stack
    TM_WRITE_TRANSACTION([&]() {
        PStack *pstack = RomulusLog::get_object<PStack>(0);
        // sgx_printf("Popped two items: %ld and %ld\n", pstack->pop(), pstack->pop());
        // This one should be "EMTPY" which is 999999999
        sgx_printf("Worker: %d Popped : %ld\n",tid, pstack->pop());
    });

    // Add items to the persistent stack
    TM_WRITE_TRANSACTION([&]() {
        PStack *pstack = RomulusLog::get_object<PStack>(0);
        pstack->push(val);
        //pstack->push(44);
    });

    // Pop two items from the persistent stack
    TM_WRITE_TRANSACTION([&]() {
        PStack *pstack = RomulusLog::get_object<PStack>(0);
        // sgx_printf("Popped two items: %ld and %ld\n", pstack->pop(), pstack->pop());
        // This one should be "EMTPY" which is 999999999
        sgx_printf("Worker: %d Pushed and Popped : %ld\n", tid, pstack->pop());
    });

    // Delete the persistent stack from persistent memory
    /*TM_WRITE_TRANSACTION([&]() {
        sgx_printf("Destroying persistent stack...\n");
        PStack *pstack = RomulusLog::get_object<PStack>(0);
        TM_PFREE(pstack);
        RomulusLog::put_object<PStack>(0, nullptr);
    });*/
}
/* 
 * sgx_printf: 
 * Invokes OCALL to display the enclave buffer to the terminal.
 */
int __cxa_thread_atexit(void (*dtor)(void *), void *obj, void *dso_symbol) {}

int sgx_printf(const char *fmt, ...)
{
    char buf[BUFSIZ] = {'\0'};
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);
    ocall_print_string(buf);
    return (int)strnlen(buf, BUFSIZ - 1) + 1;
}
void abort_h()
{
    sgx_printf("called abort from rom sgx\n");
}

void do_close()
{
    my_ocall_close();
}

void empty_ecall()
{
    sgx_printf("Inside empty ecall\n");
}
